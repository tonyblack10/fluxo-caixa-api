import database from '../../src/config/database'

database.sync({ force: true, schema: 'public', logging: false })
  .done((): void => console.log('Banco de dados sincronizado.'))
