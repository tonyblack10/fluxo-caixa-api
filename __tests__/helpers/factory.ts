/* eslint-disable @typescript-eslint/explicit-function-return-type */
import * as faker from 'faker'
import { factory } from 'factory-girl'

import Credor from '../../src/models/Credor'
import TipoDeDespesa from '../../src/models/TipoDeDespesa'
import Despesa from '../../src/models/Despesa'
import Usuario from '../../src/models/Usuario'

factory.define('credor', Credor, {
  descricao: faker.lorem.sentence(2)
})

factory.define('tipoDeDespesa', TipoDeDespesa, {
  descricao: faker.lorem.sentence(2)
})

factory.define('despesa', Despesa, {
  descricao: faker.lorem.sentence(2),
  dataDaCompetencia: new Date('2019-04-25'),
  dataDeVencimento: new Date('2019-05-10'),
  valor: faker.random.number({ min: 25, max: 150, precision: 2 }),
  credorId: factory.assoc('credor', 'id'),
  tipoDeDespesa: factory.assoc('tipoDeDespesa', 'id')
})

factory.extend('despesa', 'despesaComDadosDePagamentoValidos', {
  dataDePagamento: new Date('2019-04-30'),
  desconto: 0,
  multa: 0
})

factory.extend('despesa', 'despesaComDadosDePagamentoInvalidos', {
  dataDePagamento: new Date('2019-04-24'),
  desconto: -1,
  multa: -5
})

factory.define('usuario', Usuario, {
  nome: `${faker.name.firstName()} ${faker.name.lastName()}`,
  email: faker.internet.exampleEmail(),
  senha: '123456',
  isAdmin: false
})

factory.extend('usuario', 'usuarioAdmin', {
  isAdmin: true
})

export default factory
