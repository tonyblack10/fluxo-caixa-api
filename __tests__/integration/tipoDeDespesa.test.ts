/* eslint-disable @typescript-eslint/explicit-function-return-type */
/* eslint-disable no-undef */
import * as request from 'supertest'
import factory from '../helpers/factory'

import app from '../../src/app'

import criaToken from '../../src/helpers/criaToken'
import TipoDeDespesa from '../../src/models/TipoDeDespesa'

let token = ''

beforeAll(async () => {
  await TipoDeDespesa.destroy({ where: {} })
  token = criaToken(await factory.build('usuario'))
})

describe('TiposDeDespesas', () => {
  it('deve retornar um tipo de despesa por ID', async () => {
    const tipoDeDespesa = await factory.create('tipoDeDespesa')

    const response = await request(app)
      .get(`/api/v1/tipos-despesas/${tipoDeDespesa.id}`)
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${token}`)

    expect(response.ok).toBeTruthy()
    expect(response.body.id).toBe(tipoDeDespesa.id)
    expect(response.body.descricao).toBe(tipoDeDespesa.descricao)
  })

  it('deve retornar uma pagina de tipos de despesas', async () => {
    await factory.createMany('tipoDeDespesa', 3)

    const response = await request(app)
      .get('/api/v1/tipos-despesas')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${token}`)

    expect(response.ok).toBeTruthy()
    expect(response.body.conteudo).not.toBeNull()
    expect(response.body.total).toBeGreaterThan(0)
    expect(response.body.pagina).toBe(0)
  })

  it('deve salvar um novo tipo de despesa', async () => {
    const tipoDeDespesa = await factory.build('tipoDeDespesa')
    const response = await request(app)
      .post('/api/v1/tipos-despesas')
      .send(tipoDeDespesa.dataValues)
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${token}`)

    expect(response.status).toBe(201)
    expect(response.header.location).not.toBeNull()
  })

  it('deve retornar status 400 ao tentar salvar tipo de despesa com dados invalidos', async () => {
    const response = await request(app)
      .post('/api/v1/tipos-despesas')
      .send({ descricao: '' })
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${token}`)

    expect(response.badRequest).toBeTruthy()
    expect(response.body.hasOwnProperty('message')).toBeTruthy()
    expect(response.body.hasOwnProperty('errors')).toBeTruthy()
  })

  it('deve editar um tipo de despesa', async () => {
    const tipoDeDespesa = await factory.create('tipoDeDespesa')

    const response = await request(app)
      .put(`/api/v1/tipos-despesas/${tipoDeDespesa.id}`)
      .send(await factory.build('tipoDeDespesa').dataValues)
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${token}`)

    expect(response.noContent).toBeTruthy()
  })

  it('deve remover um tipo de despesa por ID', async () => {
    const tipoDeDespesa = await factory.create('tipoDeDespesa')

    const response = await request(app)
      .delete(`/api/v1/tipos-despesas/${tipoDeDespesa.id}`)
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${token}`)

    expect(response.noContent).toBeTruthy()
  })
})
