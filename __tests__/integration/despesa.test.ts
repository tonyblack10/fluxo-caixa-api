/* eslint-disable @typescript-eslint/explicit-function-return-type */
/* eslint-disable no-undef */
import * as request from 'supertest'
import factory from '../helpers/factory'

import app from '../../src/app'

import criaToken from '../../src/helpers/criaToken'
import Despesa from '../../src/models/Despesa'

let token = ''

beforeAll(async () => {
  await Despesa.destroy({ where: {} })
  token = criaToken(await factory.build('usuario'))
})

describe('Despesas', () => {
  it('deve retornar uma despesa por ID', async () => {
    const despesa = await factory.create('despesa')

    const response = await request(app)
      .get(`/api/v1/despesas/${despesa.id}`)
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${token}`)

    expect(response.ok).toBeTruthy()
    expect(response.body.id).toBe(despesa.id)
    expect(response.body.descricao).toBe(despesa.descricao)
    expect(response.body.valor).toBe(despesa.valor)
  })

  it('deve remover uma despesa por ID', async () => {
    const despesa = await factory.create('despesa')

    const response = await request(app)
      .delete(`/api/v1/despesas/${despesa.id}`)
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${token}`)

    expect(response.noContent).toBeTruthy()
  })

  it('deve retornar uma pagina de despesas', async () => {
    await factory.createMany('despesa', 5)

    const response = await request(app)
      .get('/api/v1/despesas')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${token}`)

    expect(response.ok).toBeTruthy()
    expect(response.body.conteudo).not.toBeNull()
    expect(response.body.total).toBeGreaterThan(0)
    expect(response.body.pagina).toBe(0)
  })

  it('deve salvar uma nova despesa', async () => {
    const despesa = await factory.build('despesa')

    const response = await request(app)
      .post('/api/v1/despesas')
      .send(despesa.dataValues)
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${token}`)

    expect(response.status).toBe(201)
    expect(response.header.location).not.toBeNull()
  })

  it('deve salvar uma nova despesa com dados de pagamento', async () => {
    const despesa = await factory.build('despesaComDadosDePagamentoValidos')

    const response = await request(app)
      .post('/api/v1/despesas')
      .send(despesa.dataValues)
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${token}`)

    expect(response.status).toBe(201)
    expect(response.header.location).not.toBeNull()
  })

  it('deve retornar status code 400 ao tentar salvar despesa com dados de pagamento invalidos', async () => {
    const despesa = await factory.build('despesaComDadosDePagamentoInvalidos')

    const response = await request(app)
      .post('/api/v1/despesas')
      .send(despesa.dataValues)
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${token}`)

    expect(response.badRequest).toBeTruthy()
    expect(response.body.hasOwnProperty('message')).toBeTruthy()
    expect(response.body.hasOwnProperty('errors')).toBeTruthy()
  })

  it('deve editar uma despesa', async () => {
    const despesa = await factory.create('despesa')
    const despesaEditada = await factory.build('despesa')

    const response = await request(app)
      .put(`/api/v1/despesas/${despesa.id}`)
      .send(despesaEditada.dataValues)
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${token}`)

    expect(response.noContent).toBeTruthy()
  })
})
