/* eslint-disable @typescript-eslint/explicit-function-return-type */
/* eslint-disable no-undef */
import * as request from 'supertest'
import factory from '../helpers/factory'

import app from '../../src/app'
import Usuario from '../../src/models/Usuario'

beforeEach(async () => {
  await Usuario.destroy({ where: {} })
})

describe('Login', () => {
  it('deve retornar status code 401 ao tentar realizar login sem enviar email e/ou senha', async () => {
    const response = await request(app)
      .post('/api/v1/login')
      .send({ email: '', senha: '' })
      .set('Content-Type', 'application/json')

    expect(response.unauthorized).toBeTruthy()
  })

  it('deve retornar status code 401 ao tentar realizar login com usuario inexistente', async () => {
    const response = await request(app)
      .post('/api/v1/login')
      .send({ email: 'emailx@email.com', senha: '123456' })
      .set('Content-Type', 'application/json')

    expect(response.unauthorized).toBeTruthy()
  })

  it('deve retornar status code 401 ao tentar realizar login com senha incorreta', async () => {
    const usuario = await factory.create('usuario')

    const response = await request(app)
      .post('/api/v1/login')
      .send({ email: usuario.email, senha: '000000' })
      .set('Content-Type', 'application/json')

    expect(response.unauthorized).toBeTruthy()
  })

  it('deve realizar login com dados validos', async () => {
    const usuario = await factory.create('usuario')

    const response = await request(app)
      .post('/api/v1/login')
      .send({ email: usuario.dataValues.email, senha: '123456' })
      .set('Content-Type', 'application/json')

    expect(response.ok).toBeTruthy()
    expect(response.header['access-token']).not.toBeNull()
  })
})
