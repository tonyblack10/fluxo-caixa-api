/* eslint-disable @typescript-eslint/explicit-function-return-type */
/* eslint-disable no-undef */
import * as request from 'supertest'
import factory from '../helpers/factory'

import app from '../../src/app'

import criaToken from '../../src/helpers/criaToken'
import Credor from '../../src/models/Credor'

let token = ''

beforeAll(async () => {
  await Credor.destroy({ where: {} })
  token = criaToken(await factory.build('usuario'))
})

describe('Credores', () => {
  it('deve retornar um credor por ID', async () => {
    const credor = await factory.create('credor')

    const response = await request(app)
      .get(`/api/v1/credores/${credor.id}`)
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${token}`)

    expect(response.ok).toBeTruthy()
    expect(response.body.id).toBe(credor.id)
    expect(response.body.descricao).toBe(credor.descricao)
  })

  it('deve retornar uma pagina de credores', async () => {
    await factory.createMany('credor', 3)

    const response = await request(app)
      .get('/api/v1/credores')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${token}`)

    expect(response.ok).toBeTruthy()
    expect(response.body.conteudo).not.toBeNull()
    expect(response.body.total).toBeGreaterThan(0)
    expect(response.body.pagina).toBe(0)
  })

  it('deve salvar um novo credor', async () => {
    const credor = await factory.build('credor')
    const response = await request(app)
      .post('/api/v1/credores')
      .send(credor.dataValues)
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${token}`)

    expect(response.status).toBe(201)
    expect(response.header.location).not.toBeNull()
  })

  it('deve retornar status 400 ao tentar salvar credor com dados invalidos', async () => {
    const response = await request(app)
      .post('/api/v1/credores')
      .send({ descricao: '' })
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${token}`)

    expect(response.badRequest).toBeTruthy()
    expect(response.body.hasOwnProperty('message')).toBeTruthy()
    expect(response.body.hasOwnProperty('errors')).toBeTruthy()
  })

  it('deve editar um credor', async () => {
    const credor = await factory.create('credor')

    const response = await request(app)
      .put(`/api/v1/credores/${credor.id}`)
      .send(await factory.build('credor').dataValues)
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${token}`)

    expect(response.noContent).toBeTruthy()
  })

  it('deve remover um credor por ID', async () => {
    const credor = await factory.create('credor')

    const response = await request(app)
      .delete(`/api/v1/credores/${credor.id}`)
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${token}`)

    expect(response.noContent).toBeTruthy()
  })
})
