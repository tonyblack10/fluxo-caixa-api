import { Op } from 'sequelize'

import { Transational } from '../decorators/Transational'
import TipoDeDespesaInterface from '../interfaces/TipoDeDespesaInterface'
import PaginaInterface from '../interfaces/PaginaInterface'
import geraDadosPaginacao from '../helpers/geraDadosPaginacao'
import TipoDeDespesa from '../models/TipoDeDespesa'

class TipoDeDespesaRepository {
  @Transational()
  public salva (tipoDeDespesa: TipoDeDespesaInterface): Promise<TipoDeDespesa> {
    return TipoDeDespesa.create(tipoDeDespesa)
  }

  public async busca (busca = '', pagina = 0, limit = 10): Promise<PaginaInterface> {
    const dadosDaPagina = geraDadosPaginacao(pagina, limit)
    const resultado = await TipoDeDespesa
      .findAndCountAll({ where: { descricao: { [Op.iLike]: `%${busca}%` } },
        order: [['descricao', 'ASC']],
        offset: dadosDaPagina['offset'],
        limit: dadosDaPagina['limit'] })

    return Promise.resolve({ conteudo: (resultado.rows as TipoDeDespesa[]),
      pagina: dadosDaPagina['pagina'],
      total: (resultado.count as number) })
  }

  public buscaPorId (id: number): Promise<TipoDeDespesa> {
    return TipoDeDespesa.findByPk(id)
  }

  @Transational()
  public edita (credor: TipoDeDespesaInterface, id: number): Promise<[number, TipoDeDespesa[]]> {
    return TipoDeDespesa.update(credor, { where: { id } })
  }

  @Transational()
  public remove (id: number): Promise<number> {
    return TipoDeDespesa.destroy({ where: { id } })
  }
}

export default new TipoDeDespesaRepository()
