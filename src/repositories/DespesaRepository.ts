import { WhereOptions } from 'sequelize'

import { Transational } from '../decorators/Transational'
import PaginaInterface from '../interfaces/PaginaInterface'
import DespesaInterface from '../interfaces/DespesaInterface'
import FiltroDespesaBuilder from '../builders/FiltroDespesaBuilder'
import geraDadosPaginacao from '../helpers/geraDadosPaginacao'
import Despesa from '../models/Despesa'
import TipoDeDespesa from '../models/TipoDeDespesa'
import Credor from '../models/Credor'

class DespesaRepository {
  @Transational()
  public salva (despesa: DespesaInterface): Promise<Despesa> {
    return Despesa.create(despesa)
  }

  public async busca (params: object, pagina = 0, limit = 10): Promise<PaginaInterface> {
    const dadosDaPagina = geraDadosPaginacao(pagina, limit)
    const where = this._montaFiltroDeBusca(params)

    const resultado = await Despesa
      .findAndCountAll({
        where,
        order: [['dataDeVencimento', 'DESC']],
        attributes: { exclude: ['tipoDeDespesaId', 'credorId'] },
        offset: dadosDaPagina['offset'],
        limit: dadosDaPagina['limit'],
        include: [{ model: Credor }, { model: TipoDeDespesa }]
      })

    return Promise.resolve({ conteudo: (resultado.rows as Credor[]),
      pagina: dadosDaPagina['pagina'],
      total: (resultado.count as number) })
  }

  public buscaPorId (id: number): Promise<Despesa> {
    return Despesa.findByPk(id, { include: [{ model: Credor }, { model: TipoDeDespesa }],
      attributes: { exclude: ['tipoDeDespesaId', 'credorId'] } })
  }

  @Transational()
  public edita (despesa: DespesaInterface, id: number): Promise<[number, Despesa[]]> {
    return Despesa.update(despesa, { where: { id } })
  }

  @Transational()
  public remove (id: number): Promise<number> {
    return Despesa.destroy({ where: { id } })
  }

  private _montaFiltroDeBusca (params: object): WhereOptions {
    return new FiltroDespesaBuilder()
      .comDescricao(params['descricao'])
      .doCredor(params['credorId'])
      .doTipo(params['tipoId'])
      .comStatusPagamento(params['status'])
      .comVencimentoEntre(params['dataInicio'], params['dataFim'])
      .build()
  }
}

export default new DespesaRepository()
