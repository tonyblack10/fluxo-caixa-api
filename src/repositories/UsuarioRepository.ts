import { Op } from 'sequelize'
import * as bcrypt from 'bcryptjs'

import { Transational } from '../decorators/Transational'
import UsuarioInterface from '../interfaces/UsuarioInterface'
import PaginaInterface from '../interfaces/PaginaInterface'
import Usuario from '../models/Usuario'

class UsuarioRepository {
  @Transational()
  public salva (usuario: UsuarioInterface): Promise<Usuario> {
    return Usuario.create(usuario)
  }

  public async busca (busca = '', pagina = 0, limit = 10): Promise<PaginaInterface> {
    const offset = pagina * limit
    const resultado = await Usuario
      .findAndCountAll({ where: { nome: { [Op.iLike]: `%${busca}%` } },
        order: [['nome', 'ASC']],
        offset,
        attributes: { exclude: ['senha'] },
        limit })

    return Promise.resolve({ conteudo: (resultado.rows as Usuario[]), pagina, total: (resultado.count as number) })
  }

  public buscaPorId (id: number): Promise<Usuario> {
    return Usuario.findByPk(id, { attributes: { exclude: ['senha'] } })
  }

  public buscaPorEmail (email: string): Promise<Usuario> {
    return Usuario.findOne({ where: { email: { [Op.eq]: email } } })
  }

  @Transational()
  public edita (usuario: UsuarioInterface, id: number): Promise<[number, Usuario[]]> {
    return Usuario.update(usuario, { where: { id } })
  }

  @Transational()
  public async alteraSenha (senha: string, id: number): Promise<[number, Usuario[]]> {
    const hash = bcrypt.hash(senha, 10)
    return Usuario.update({ senha: hash }, { where: { id } })
  }

  @Transational()
  public remove (id: number): Promise<number> {
    return Usuario.destroy({ where: { id } })
  }
}

export default new UsuarioRepository()
