import { Op } from 'sequelize'

import { Transational } from '../decorators/Transational'
import CredorInterface from '../interfaces/CredorInterface'
import PaginaInterface from '../interfaces/PaginaInterface'
import geraDadosPaginacao from '../helpers/geraDadosPaginacao'
import Credor from '../models/Credor'

class CredorRepository {
  @Transational()
  public salva (credor: CredorInterface): Promise<Credor> {
    return Credor.create(credor)
  }

  public async busca (busca = '', pagina = 0, limit = 10): Promise<PaginaInterface> {
    const dadosDaPagina = geraDadosPaginacao(pagina, limit)
    const resultado = await Credor
      .findAndCountAll({ where: { descricao: { [Op.iLike]: `%${busca}%` } },
        order: [['descricao', 'ASC']],
        offset: dadosDaPagina['offset'],
        limit: dadosDaPagina['limit'] })

    return Promise.resolve({ conteudo: (resultado.rows as Credor[]),
      pagina: dadosDaPagina['pagina'],
      total: (resultado.count as number) })
  }

  public buscaPorId (id: number): Promise<Credor> {
    return Credor.findByPk(id)
  }

  @Transational()
  public edita (credor: CredorInterface, id: number): Promise<[number, Credor[]]> {
    return Credor.update(credor, { where: { id } })
  }

  @Transational()
  public remove (id: number): Promise<number> {
    return Credor.destroy({ where: { id } })
  }
}

export default new CredorRepository()
