import { Model } from 'sequelize/types'

export default interface PaginaInterface {
  conteudo: Model[],
  pagina: number,
  total: number
}
