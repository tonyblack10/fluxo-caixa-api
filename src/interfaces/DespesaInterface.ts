export default interface DespesaInterface {
  descricao: string,
  dataDaCompetencia: Date,
  dataDeVencimento: Date,
  valor: number,
  dataDePagamento: Date,
  desconto: number,
  multa: number,
  credorId: number,
  tipoDeDespesa: number
}
