export default interface UsuarioInterface {
  nome?: string,
  email?: string,
  senha?: string,
  isAdmin?: boolean
}
