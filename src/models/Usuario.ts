/* eslint-disable @typescript-eslint/explicit-function-return-type */
import { Table, Model, Column, PrimaryKey, AutoIncrement, NotEmpty, Length,
  DataType, IsEmail, Is, BeforeCreate
} from 'sequelize-typescript'

import * as bcrypt from 'bcryptjs'

import { ehEmailRepetidoValidator } from '../validators/usuario/ehEmailRepetidoValidator'

@Table({
  tableName: 'usuarios',
  timestamps: false
})
export default class Usuario extends Model<Usuario> {
  @PrimaryKey @AutoIncrement
  @Column
  public id: number

  @NotEmpty
  @Length({ min: 5, max: 200 })
  @Column({ allowNull: false, type: DataType.STRING(200) })
  public nome: string

  @IsEmail
  @Length({ max: 255 })
  @Is('ehEmailRepetidoValidator', ehEmailRepetidoValidator)
  @Column({ allowNull: false, type: DataType.STRING(255), unique: true })
  public email: string

  @Length({ min: 6, max: 8 })
  @Column({ allowNull: false, type: DataType.STRING(255) })
  public senha: string

  @Column({ type: DataType.BOOLEAN, defaultValue: false, field: 'is_admin' })
  public isAdmin: boolean

  @BeforeCreate
  private static async criptografaSenha (instance: Usuario) {
    if (instance.senha.length < 9) {
      instance.senha = await bcrypt.hash(instance.senha, 10)
    }
  }

  public static isSenhaCorreta (senhaTexto: string, senhaHash: string): Promise<boolean> {
    return bcrypt.compare(senhaTexto, senhaHash)
  }
}
