/* eslint-disable @typescript-eslint/explicit-function-return-type */
import {
  Table, Model, Column, PrimaryKey, AutoIncrement, NotEmpty, Length,
  BelongsTo, ForeignKey, DataType, IsDate, IsNumeric, Min, Is
} from 'sequelize-typescript'

import TipoDeDespesa from './TipoDeDespesa'
import Credor from './Credor'
import { ehMaiorOuIgualADataDeCompetenciaValidator } from '../validators/despesa'

@Table({
  tableName: 'despesas',
  timestamps: false
})
export default class Despesa extends Model<Despesa> {
  @PrimaryKey @AutoIncrement
  @Column
  public id: number

  @NotEmpty
  @Length({ min: 5, max: 200 })
  @Column
  public descricao: string

  @NotEmpty
  @IsDate
  @Column({ field: 'data_competencia', allowNull: false, type: DataType.DATEONLY })
  public dataDaCompetencia: Date

  @NotEmpty
  @IsDate
  @Is('ehMenorQueDataDeCompetencia', ehMaiorOuIgualADataDeCompetenciaValidator)
  @Column({ field: 'data_vencimento', allowNull: false, type: DataType.DATEONLY })
  public dataDeVencimento: Date

  @NotEmpty
  @IsNumeric
  @Min(1)
  @Column({ allowNull: false, type: DataType.DECIMAL(8, 2) })
  public valor: number

  @IsDate
  @Is('ehMaiorOuIgualADataDeCompetencia', ehMaiorOuIgualADataDeCompetenciaValidator)
  @Column({ field: 'data_pagamento', allowNull: true, type: DataType.DATEONLY })
  public dataDePagamento: Date

  @IsNumeric
  @Min(0)
  @Column({ allowNull: false, type: DataType.DECIMAL(8, 2), defaultValue: '0.0' })
  public desconto: number

  @IsNumeric
  @Min(0)
  @Column({ allowNull: false, type: DataType.DECIMAL(8, 2), defaultValue: '0.0' })
  public multa: number

  @ForeignKey(() => TipoDeDespesa)
  @Column({ field: 'tipo_despesa_id' })
  public tipoDeDespesaId: number

  @BelongsTo(() => TipoDeDespesa)
  public tipoDeDespesa: TipoDeDespesa

  @ForeignKey(() => Credor)
  @Column({ field: 'credor_id' })
  public credorId: number

  @BelongsTo(() => Credor)
  public credor: Credor
}
