import { Table, Model, Column, PrimaryKey, AutoIncrement, NotEmpty, Length, HasMany } from 'sequelize-typescript'

import Despesa from './Despesa'

@Table({
  tableName: 'tipos_despesas',
  timestamps: false
})
export default class TipoDeDespesa extends Model<TipoDeDespesa> {
  @PrimaryKey @AutoIncrement
  @Column
  public id: number

  @NotEmpty
  @Length({ min: 5, max: 200 })
  @Column
  public descricao: string

  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  @HasMany(() => Despesa)
  public despesas: Despesa[]
}
