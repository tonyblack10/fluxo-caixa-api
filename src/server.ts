import app from './app'
import database from './config/database'

// eslint-disable-next-line @typescript-eslint/no-unused-vars
process.on('unhandledRejection', function (reason: {} | null | undefined, promise: Promise<any>): void {})

database.sync({ force: false }).done((): void => {
  app.listen(3000)
})
