import * as moment from 'moment'

export const ehMaiorOuIgualADataDeCompetenciaValidator = function (value): void {
  if (value || this.dataDaCompetencia) {
    if (moment(value, ['YYYY-MM-DD']).isSameOrBefore(moment(this.dataDaCompetencia, ['YYYY-MM-DD']))) {
      throw new Error('A data de pagamento não pode ser menor ou igual a data de competência')
    }
  }
}
