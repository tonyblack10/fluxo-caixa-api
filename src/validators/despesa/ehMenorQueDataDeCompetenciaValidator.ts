import * as moment from 'moment'

export const ehMenorQueDataDeCompetenciaValidator = function (value: Date): void {
  if (!value || !this.dataDaCompetencia) {
    return
  }
  if (moment(value, ['YYYY-MM-DD']).isBefore(moment(this.dataDaCompetencia, ['YYYY-MM-DD']))) {
    throw new Error('A data de vencimento não pode ser menor do que a data de competência')
  }
}
