import Usuario from '../../models/Usuario'

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export const ehEmailRepetidoValidator = async function (value: string) {
  if (value) {
    const usuario = await Usuario.findOne({ where: { email: value } })
    if (usuario && usuario.id !== this.id) {
      throw new Error('Este e-mail já existe no banco de dados')
    }
  }
}
