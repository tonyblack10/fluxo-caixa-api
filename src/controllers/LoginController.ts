import { Request, Response } from 'express'

import criaToken from '../helpers/criaToken'
import Usuario from '../models/Usuario'

class LoginController {
  public async login (req: Request, res: Response): Promise<Response> {
    const { email, senha } = req.body
    if (!email || !senha) {
      return res.sendStatus(401)
    } else {
      try {
        const usuario = await Usuario.findOne({ where: { email } })
        if (!usuario) {
          return res.sendStatus(401)
        }
        const senhaValida = await Usuario.isSenhaCorreta(senha, usuario.senha)

        if (!senhaValida) {
          return res.sendStatus(401)
        } else {
          const token = criaToken(usuario)

          res.set('x-access-token', token)
          return res.sendStatus(200)
        }
      } catch (err) {
        return res.sendStatus(401)
      }
    }
  }
}

export default new LoginController()
