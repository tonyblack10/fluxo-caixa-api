import { Request, Response } from 'express'

import validationErrorHandler from '../middlewares/validationErrorHandler'
import repository from '../repositories/UsuarioRepository'
import UsuarioInterface from '../interfaces/UsuarioInterface'
import Usuario from '../models/Usuario'

class UsuarioController {
  public async index (req: Request, res: Response): Promise<Response> {
    try {
      const { busca, pagina, limit } = req.query
      const resultado = await repository.busca(busca, pagina, limit)

      return res.json(resultado)
    } catch (err) {
      return res.status(500).json(err)
    }
  }

  public async salva (req: Request, res: Response): Promise<Response> {
    try {
      const body = req.body as UsuarioInterface
      await Usuario.build(body).validate()
      const usuario = await repository.salva(body)
      res.location(`/api/v1/usuarios/${usuario.id}`)
      return res.sendStatus(201)
    } catch (err) {
      return validationErrorHandler(err, res)
    }
  }

  public async buscaPorId (req: Request, res: Response): Promise<Response> {
    try {
      const { id } = req.params
      const usuario = await Usuario.findOne({ where: { id }, attributes: { exclude: ['senha'] } })
      if (!usuario) {
        return res.sendStatus(404)
      }

      return res.json(usuario)
    } catch (err) {
      return res.status(500).json(err)
    }
  }

  public async edita (req: Request, res: Response): Promise<Response> {
    try {
      const { id } = req.params
      const body: UsuarioInterface = { nome: req.body.nome, email: req.body.email }
      await Usuario.build(body).validate({ fields: ['nome', 'email'] })
      repository.edita(body, id)

      return res.sendStatus(204)
    } catch (err) {
      return validationErrorHandler(err, res)
    }
  }

  public async alteraSenha (req: Request, res: Response): Promise<Response> {
    try {
      const { id } = req.params
      const usuarioLogado = await repository.buscaPorEmail(req['email'])
      if (id !== usuarioLogado.id) {
        return res.status(401).send({ erro: 'Os usuários podem alterar somente sua própria senha!' })
      }
      const { senha } = req.body
      await Usuario.build({ senha }).validate({ fields: ['senha'] })

      await repository.alteraSenha(senha, id)

      return res.sendStatus(204)
    } catch (err) {
      return res.status(500).json(err)
    }
  }

  public async remove (req: Request, res: Response): Promise<Response> {
    try {
      const { id } = req.params
      await repository.remove(id)
      return res.sendStatus(204)
    } catch (err) {
      return res.status(500).json(err)
    }
  }
}

export default new UsuarioController()
