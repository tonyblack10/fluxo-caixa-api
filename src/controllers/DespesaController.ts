import { Request, Response } from 'express'

import DespesaInterface from '../interfaces/DespesaInterface'
import repository from '../repositories/DespesaRepository'
import validationErrorHandler from '../middlewares/validationErrorHandler'
import Despesa from '../models/Despesa'

class DespesaController {
  public async index (req: Request, res: Response): Promise<Response> {
    try {
      const { pagina, limit } = req.query
      const resultado = await repository.busca(req.query, pagina, limit)
      return res.json(resultado)
    } catch (err) {
      return res.status(500).json(err)
    }
  }

  public async salva (req: Request, res: Response): Promise<Response> {
    try {
      const body = req.body as DespesaInterface
      await Despesa.build(body).validate()
      const despesa = await repository.salva(body)
      res.location(`/api/v1/despesas/${despesa.id}`)
      return res.sendStatus(201)
    } catch (err) {
      return validationErrorHandler(err, res)
    }
  }

  public async buscaPorId (req: Request, res: Response): Promise<Response> {
    try {
      const { id } = req.params
      const despesa = await repository.buscaPorId(id)

      if (!despesa) {
        return res.sendStatus(404)
      }

      return res.json(despesa)
    } catch (err) {
      return res.status(500).json(err)
    }
  }

  public async edita (req: Request, res: Response): Promise<Response> {
    try {
      const { id } = req.params
      const body = req.body as DespesaInterface
      await Despesa.build(body).validate()
      await repository.edita(body, id)

      return res.sendStatus(204)
    } catch (err) {
      return validationErrorHandler(err, res)
    }
  }

  public async remove (req: Request, res: Response): Promise<Response> {
    try {
      const { id } = req.params
      await repository.remove(id)
      return res.sendStatus(204)
    } catch (err) {
      return res.status(500).json(err)
    }
  }
}

export default new DespesaController()
