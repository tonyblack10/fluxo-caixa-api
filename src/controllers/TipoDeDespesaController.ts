import { Request, Response } from 'express'

import repository from '../repositories/TipoDeDespesaRepository'
import validationErrorHandler from '../middlewares/validationErrorHandler'
import TipoDeDespesa from '../models/TipoDeDespesa'

class TipoDeDespesaController {
  public async index (req: Request, res: Response): Promise<Response> {
    try {
      const { busca, pagina, limit } = req.query
      const resultado = await repository.busca(busca, pagina, limit)
      return res.json(resultado)
    } catch (err) {
      return res.status(500).json(err)
    }
  }

  public async salva (req: Request, res: Response): Promise<Response> {
    try {
      const { descricao } = req.body
      await TipoDeDespesa.build({ descricao }).validate()
      const tipoDeDespesa = await repository.salva({ descricao })
      res.location(`/api/v1/tipos-despesas/${tipoDeDespesa.id}`)
      return res.sendStatus(201)
    } catch (err) {
      return validationErrorHandler(err, res)
    }
  }

  public async buscaPorId (req: Request, res: Response): Promise<Response> {
    try {
      const { id } = req.params
      const tipoDeDespesa = await repository.buscaPorId(id)
      if (!tipoDeDespesa) {
        return res.sendStatus(404)
      }
      return res.json(tipoDeDespesa)
    } catch (err) {
      return res.status(500).json(err)
    }
  }

  public async edita (req: Request, res: Response): Promise<Response> {
    try {
      const { id } = req.params
      const { descricao } = req.body
      await TipoDeDespesa.build({ descricao }).validate()
      await repository.edita({ descricao }, id)

      return res.sendStatus(204)
    } catch (err) {
      return validationErrorHandler(err, res)
    }
  }

  public async remove (req: Request, res: Response): Promise<Response> {
    try {
      const { id } = req.params
      await repository.remove(id)
      return res.sendStatus(204)
    } catch (err) {
      return res.status(500).json(err)
    }
  }
}

export default new TipoDeDespesaController()
