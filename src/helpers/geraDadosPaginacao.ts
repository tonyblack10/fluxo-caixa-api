const QTD_MAX_ELEMENTOS_POR_PAGINA = 20
const QTD_PADRAO_DE_ELEMENTOS_POR_PAGINA = 10

export default function (pagina: number, limit: number): object {
  if (pagina === undefined) {
    pagina = 0
  }

  if (limit === undefined || limit < 0) {
    limit = QTD_PADRAO_DE_ELEMENTOS_POR_PAGINA
  } else {
    if (limit > QTD_MAX_ELEMENTOS_POR_PAGINA) {
      limit = QTD_MAX_ELEMENTOS_POR_PAGINA
    }
  }

  const offset = pagina * limit

  return { pagina, limit, offset }
}
