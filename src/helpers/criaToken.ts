import * as jwt from 'jsonwebtoken'

import Usuario from '../models/Usuario'

export default function (usuario: Usuario): string {
  const jwtSecret = process.env.JWT_SECRET ? process.env.JWT_SECRET : ''
  const expiresIn = process.env.JWT_EXPIRATION ? process.env.JWT_EXPIRATION : ''

  return jwt.sign({
    nome: usuario.nome,
    email: usuario.email,
    admin: usuario.isAdmin
  }, jwtSecret, { expiresIn })
}
