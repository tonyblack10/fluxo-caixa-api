import { Op, WhereOptions } from 'sequelize'

export default class FiltroDespesaBuilder {
  private _filtro: WhereOptions

  public constructor () {
    this._filtro = {}
  }

  public doCredor (credorId: number): FiltroDespesaBuilder {
    if (credorId) {
      this._filtro['credores_id'] = { [Op.eq]: credorId }
    }
    return this
  }

  public doTipo (tipoId: number): FiltroDespesaBuilder {
    if (tipoId) {
      this._filtro['tipos_despesas_id'] = { [Op.eq]: tipoId }
    }
    return this
  }

  public comVencimentoEntre (dataInicio: string, dataFim: string): FiltroDespesaBuilder {
    if (dataInicio && dataFim) {
      this._filtro['dataDeVencimento'] = { [Op.between]: [dataInicio, dataFim] }
    }

    return this
  }

  public comStatusPagamento (status: string | boolean): FiltroDespesaBuilder {
    if (status !== undefined) {
      status = status === 'true'
      if (status) {
        this._filtro['dataDePagamento'] = { [Op.not]: null }
      } else {
        this._filtro['dataDePagamento'] = { [Op.eq]: null }
      }
    }

    return this
  }

  public comDescricao (descricao: string): FiltroDespesaBuilder {
    if (descricao) {
      this._filtro['descricao'] = { [Op.iLike]: descricao }
    }

    return this
  }

  public build (): WhereOptions {
    return this._filtro
  }
}
