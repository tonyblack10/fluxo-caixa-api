import { Sequelize } from 'sequelize-typescript'
import * as path from 'path'
import * as dotenv from 'dotenv'

if (process.env.NODE_ENV === 'test') {
  dotenv.config({ path: '.env.test' })
} else {
  dotenv.config()
}

const database = process.env.DB_NOME || ''
const username = process.env.DB_USUARIO || ''
const password = process.env.DB_SENHA || ''
const host = process.env.DB_HOST || ''
const port = process.env.DB_PORTA ? parseInt(process.env.DB_PORTA) : 5432

const sequelize = new Sequelize({
  dialect: 'postgres',
  database,
  username,
  password,
  port,
  host,
  pool: {},
  logging: false,
  define: {
    charset: 'utf8'
  },
  modelPaths: [path.join(__dirname, '../models')]
})

export default sequelize
