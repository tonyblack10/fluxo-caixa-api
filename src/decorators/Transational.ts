import { Transaction } from 'sequelize/types'
import sequelize from '../config/database'

/* eslint-disable @typescript-eslint/no-explicit-any */
export function Transational (): Function {
  return function (target: any, propertyKey: string, descriptor: PropertyDescriptor): PropertyDescriptor {
    const metodoOriginal = descriptor.value

    descriptor.value = async function (...args: any[]): Promise<any> {
      const transaction: Transaction = await sequelize.transaction()
      try {
        const retorno = await metodoOriginal.apply(this, args)
        await transaction.commit()
        return retorno
      } catch (err) {
        await transaction.rollback()
        throw err
      }
    }

    return descriptor
  }
}
