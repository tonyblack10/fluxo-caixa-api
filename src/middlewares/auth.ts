import { Request, Response, NextFunction } from 'express'
import * as jwt from 'jsonwebtoken'

export default function (req: Request, res: Response, next: NextFunction): Response | void {
  const jwtSecret = process.env.JWT_SECRET ? process.env.JWT_SECRET : ''
  const authHeader = req.headers.authorization
  if (!authHeader) {
    return res.status(401).send({ erro: 'Nenhum token enviado!' })
  }
  const partes = authHeader.split(' ')

  if (partes.length !== 2) {
    return res.status(401).send({ erro: 'Token inválido!' })
  }

  const [ scheme, token ] = partes

  if (!/^Bearer$/i.test(scheme)) {
    return res.status(401).send({ erro: 'Token inválido!' })
  }

  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  jwt.verify(token, jwtSecret, (err: jwt.VerifyErrors, decoded: object | string) => {
    if (err) {
      return res.status(401).send({ erro: 'Token inválido!' })
    }

    req['email'] = decoded['email']

    return next()
  })
}
