/* eslint-disable @typescript-eslint/explicit-function-return-type */
import { Request, Response, NextFunction } from 'express'

import repository from '../repositories/UsuarioRepository'

export default async function (req: Request, res: Response, next: NextFunction) {
  const usuarioLogado = await repository.buscaPorEmail(req['email'])
  if (!usuarioLogado.isAdmin) {
    return res.status(403).send({ erro: 'Usuário não possui autorização para realizar esta ação!' })
  }

  return next()
}
