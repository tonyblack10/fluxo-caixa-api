import { Request, Response, NextFunction } from 'express'

const removeIdDoBody = (req: Request, res: Response, next: NextFunction): void => {
  delete req.body.id

  next()
}

export default removeIdDoBody
