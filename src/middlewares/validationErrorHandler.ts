import { Response } from 'express'

const SEQUELIZE_ERRORS = [
  'SequelizeValidationError',
  'SequelizeUniqueConstraintError',
  'SequelizeDatabaseError'
]

export default function (err, res: Response): Response {
  if (SEQUELIZE_ERRORS.includes(err.name)) {
    // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
    const errors = err.errors.map((e: object) => {
      return { message: e['message'], path: e['path'] }
    })
    return res.status(400).json({ message: 'Validation Errors', errors })
  }

  return res.status(500).json(err)
};
