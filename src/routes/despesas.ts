import { Router } from 'express'

import DespesaController from '../controllers/DespesaController'
import auth from '../middlewares/auth'

export default function (router: Router): void {
  router.route('/api/v1/despesas')
    .all(auth)
    .get(DespesaController.index)
    .post(DespesaController.salva)

  router.route('/api/v1/despesas/:id')
    .all(auth)
    .get(DespesaController.buscaPorId)
    .put(DespesaController.edita)
    .delete(DespesaController.remove)
}
