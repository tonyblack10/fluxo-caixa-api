import { Router } from 'express'

import LoginController from '../controllers/LoginController'

export default function (router: Router): void {
  router.post('/api/v1/login', LoginController.login)
}
