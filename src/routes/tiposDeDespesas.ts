import { Router } from 'express'

import TipoDeDespesaController from '../controllers/TipoDeDespesaController'
import auth from '../middlewares/auth'

export default function (router: Router): void {
  router.route('/api/v1/tipos-despesas')
    .all(auth)
    .get(TipoDeDespesaController.index)
    .post(TipoDeDespesaController.salva)

  router.route('/api/v1/tipos-despesas/:id')
    .all(auth)
    .get(TipoDeDespesaController.buscaPorId)
    .put(TipoDeDespesaController.edita)
    .delete(TipoDeDespesaController.remove)
}
