import { Router } from 'express'

import UsuarioController from '../controllers/UsuarioController'
import auth from '../middlewares/auth'
import isAdmin from '../middlewares/isAdmin'

export default function (router: Router): void {
  router.route('/api/v1/usuarios')
    .all(auth, isAdmin)
    .get(UsuarioController.index)
    .post(UsuarioController.salva)

  router.route('/api/v1/usuarios/:id')
    .all(auth, isAdmin)
    .get(UsuarioController.buscaPorId)
    .put(UsuarioController.edita)
    .delete(UsuarioController.remove)

  router.patch('/api/v1/usuarios/:id/senha', auth, UsuarioController.alteraSenha)
}
