import { Router } from 'express'

import CredorController from '../controllers/CredorController'
import auth from '../middlewares/auth'

export default function (router: Router): void {
  router.route('/api/v1/credores')
    .all(auth)
    .get(CredorController.index)
    .post(CredorController.salva)

  router.route('/api/v1/credores/:id')
    .all(auth)
    .get(CredorController.buscaPorId)
    .put(CredorController.edita)
    .delete(CredorController.remove)
}
