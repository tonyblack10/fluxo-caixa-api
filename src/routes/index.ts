import { Router } from 'express'

import credoresRoutes from './credores'
import tiposDeDespesasRoutes from './tiposDeDespesas'
import despesasRoutes from './despesas'
import usuariosRoutes from './usuarios'
import loginRoutes from './login'

const routes = Router()

credoresRoutes(routes)
tiposDeDespesasRoutes(routes)
despesasRoutes(routes)
usuariosRoutes(routes)
loginRoutes(routes)

export default routes
