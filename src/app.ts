import * as express from 'express'
import * as cors from 'cors'
import * as compression from 'compression'
import * as helmet from 'helmet'
import * as dotenv from 'dotenv'

import routes from './routes'
import removeIdDoBody from './middlewares/removeIdDoBody'

if (process.env.NODE_ENV === 'test') {
  dotenv.config({ path: '.env.test' })
} else {
  dotenv.config()
}

class App {
  public express: express.Application

  public constructor () {
    this.express = express()
    this.middlewares()
    this.routes()
  }

  private middlewares (): void {
    this.express.use(express.json())
    this.express.use(cors({
      allowedHeaders: ['x-access-token', 'Content-Type']
    }))
    this.express.use(removeIdDoBody)
    this.express.use(compression())
    this.express.use(helmet())
  }

  private routes (): void {
    this.express.use(routes)
  }
}

export default new App().express
